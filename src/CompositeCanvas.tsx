import React from "react";
import { CanvasElement, drawLine, Line } from "./TelestrationCanvas";

interface Props {
  width: number;
  height: number;
  videoElementRef: React.RefObject<HTMLVideoElement>;
  isCompositing: boolean;
  history: Line[];
}

export default function CompositeCanvas({
  width,
  height,
  videoElementRef,
  isCompositing,
  history,
}: Props): JSX.Element {
  const canvasRef = React.useRef<CanvasElement>(null);
  const isCompositingRef = React.useRef<boolean>(isCompositing);

  React.useEffect(() => {
    isCompositingRef.current = isCompositing;
  }, [isCompositing]);

  React.useEffect(() => {
    if (!canvasRef.current || !videoElementRef.current || !isCompositing) {
      return;
    }

    const ctx = canvasRef.current.getContext("2d");

    if (!ctx) {
      return;
    }

    const video = videoElementRef.current;
    const currentHistory: Line[] = [];

    const onRedraw = () => {
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        ctx.drawImage(video, 0, 0, width, height);

        currentHistory.forEach((line) => drawLine(ctx, line, width, height));
      }

      if (
        canvasRef.current &&
        videoElementRef.current &&
        isCompositingRef.current
      ) {
        requestAnimationFrame(onRedraw);
      }
    };

    requestAnimationFrame(onRedraw);

    const strokes = history.map((line) =>
      setTimeout(() => {
        currentHistory.push(line);
      }, line.when)
    );

    return () => {
      strokes.forEach((stroke) => {
        clearTimeout(stroke);
      });
    };
  }, [canvasRef, videoElementRef, width, height, isCompositing, history]);

  React.useEffect(() => {
    if (!isCompositing || !canvasRef.current) {
      return;
    }

    const stream = canvasRef.current.captureStream();
    const options = { mimeType: "video/webm; codecs=vp9" };
    const mediaRecorder = new MediaRecorder(stream, options);

    mediaRecorder.ondataavailable = (event: BlobEvent) => {
      const url = URL.createObjectURL(event.data);
      console.log(url);
      const a = document.createElement("a");
      a.href = url;
      a.setAttribute("download", "video.webm");
      a.click();
      window.URL.revokeObjectURL(url);
    };

    mediaRecorder.start();

    return () => {
      mediaRecorder.stop();
      stream.getTracks().forEach((track) => track.stop());
    };
  }, [canvasRef, isCompositing]);

  return <canvas width={width} height={height} ref={canvasRef} />;
}
