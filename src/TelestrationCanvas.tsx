import React from "react";

export const LINE_COLOR = "#FFFF00";
export const LINE_WIDTH = 6;

export interface Position {
  x: number;
  y: number;
}
export interface Line {
  from: Position;
  to: Position;
  when: number;
}

interface Props {
  width: number;
  height: number;
  onPenStrokes: (history: Line[]) => void;
  stream?: MediaStream;
  isRecording?: boolean;
  recordingStartedAt?: number;
}

export interface CanvasElement extends HTMLCanvasElement {
  captureStream(): MediaStream;
}

export function drawLine(
  ctx: CanvasRenderingContext2D,
  line: Line,
  width: number,
  height: number
): void {
  ctx.beginPath();

  ctx.lineWidth = LINE_WIDTH;
  ctx.lineCap = "round";
  ctx.strokeStyle = LINE_COLOR;

  ctx.moveTo((line.from.x / width) * width, (line.from.y / height) * height);
  ctx.lineTo((line.to.x / width) * width, (line.to.y / height) * height);

  ctx.stroke();
}

export default function TelestrationCanvas({
  width,
  height,
  onPenStrokes,
  stream,
  isRecording,
  recordingStartedAt,
}: Props): JSX.Element {
  const canvasRef = React.useRef<CanvasElement>(null);
  const positionRef = React.useRef<Position>({ x: 0, y: 0 });
  const historyRef = React.useRef<Line[]>([]);

  React.useEffect(() => {
    if (!canvasRef.current || !stream) {
      return;
    }

    const ctx = canvasRef.current.getContext("2d");

    if (!ctx) {
      return;
    }

    const videoTrack = stream.getVideoTracks()[0];
    if (!videoTrack) {
      return;
    }

    const imageCapture = new ImageCapture(videoTrack);

    const onRedraw = () => {
      imageCapture
        .grabFrame()
        .then((imageBitmap) => ctx.drawImage(imageBitmap, 0, 0, width, height))
        .then(() => {
          if (!historyRef.current) {
            return;
          }

          historyRef.current.forEach((line: Line) =>
            drawLine(ctx, line, width, height)
          );
        })
        .catch(() => {})
        .finally(() => {
          if (canvasRef.current) {
            requestAnimationFrame(onRedraw);
          }
        });
    };

    requestAnimationFrame(onRedraw);
  }, [canvasRef, stream, width, height]);

  React.useEffect(() => {
    if (!isRecording) {
      if (historyRef.current?.length > 0) {
        onPenStrokes(historyRef.current);
      }
      historyRef.current = [];
    }
  }, [isRecording, onPenStrokes]);

  const onMouseMove = (
    event: React.MouseEvent<HTMLCanvasElement, MouseEvent>
  ) => {
    if (!isRecording || !recordingStartedAt) {
      return;
    }

    if (event.buttons !== 1) {
      return;
    }

    const from: Position = {
      x: positionRef.current.x,
      y: positionRef.current.y,
    };

    setPosition(event);

    const to: Position = {
      x: positionRef.current.x,
      y: positionRef.current.y,
    };

    historyRef.current = [
      ...historyRef.current,
      {
        from,
        to,
        when: performance.now() - recordingStartedAt,
      },
    ];
  };

  const setPosition = (
    event: React.MouseEvent<HTMLCanvasElement, MouseEvent>
  ): void => {
    if (!isRecording) {
      return;
    }
    const position = getMousePosition(event);
    positionRef.current = position;
  };

  const getMousePosition = (
    event: React.MouseEvent<HTMLCanvasElement, MouseEvent>
  ): Position => {
    const xOffset = canvasRef.current ? canvasRef.current.offsetLeft : 0;
    const yOffset = canvasRef.current ? canvasRef.current.offsetTop : 0;

    return { x: event.clientX - xOffset, y: event.clientY - yOffset };
  };

  return (
    <canvas
      width={width}
      height={height}
      ref={canvasRef}
      onMouseMove={onMouseMove}
      onMouseDown={setPosition}
      onMouseUp={setPosition}
    />
  );
}
