import React from "react";
import {
  makeStyles,
  Theme,
  CssBaseline,
  Container,
  Grid,
  Typography,
  FormControlLabel,
  Switch,
  Card,
  CardMedia,
  CardHeader,
  CircularProgress,
} from "@material-ui/core";
import TelestrationCanvas, { Line } from "./TelestrationCanvas";
import CompositeCanvas from "./CompositeCanvas";

const VIDEO_WIDTH = 552;
const VIDEO_HEIGHT = 310;
const DESIRED_VIDEO_FPS = 15;

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(4, 0, 6),
  },
  buttons: {
    paddingBottom: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
  },
}));

function App() {
  const classes = useStyles();
  const [isRecording, setIsRecording] = React.useState<boolean>(false);
  const [recordingStartedAt, setRecordingStartedAt] = React.useState<number>();
  const [isCompositing, setIsCompositing] = React.useState<boolean>(false);
  const [localVideoStream, setLocalVideoStream] = React.useState<MediaStream>();
  const videoElementRef = React.useRef<HTMLVideoElement>(null);
  const playbackVideoElementRef = React.useRef<HTMLVideoElement>(null);
  const [lastHistory, setLastHistory] = React.useState<Line[]>([]);

  React.useEffect(() => {
    const openVideo = async () => {
      const constraints = {
        audio: false,
        video: {
          width: VIDEO_WIDTH,
          height: VIDEO_HEIGHT,
          frameRate: DESIRED_VIDEO_FPS,
        },
      };

      const localVideoStream = await navigator.mediaDevices.getUserMedia(
        constraints
      );
      setLocalVideoStream(localVideoStream);
    };

    openVideo();
  }, []);

  React.useEffect(() => {
    if (localVideoStream && videoElementRef.current) {
      videoElementRef.current.srcObject = localVideoStream;
    }
  }, [localVideoStream, videoElementRef]);

  React.useEffect(() => {
    if (!localVideoStream || !isRecording) {
      return;
    }

    const options = { mimeType: "video/webm; codecs=vp9" };
    const mediaRecorder = new MediaRecorder(localVideoStream, options);

    mediaRecorder.ondataavailable = (event: BlobEvent) => {
      if (playbackVideoElementRef.current) {
        playbackVideoElementRef.current.src = URL.createObjectURL(event.data);
      }
    };
    mediaRecorder.start();

    return () => {
      mediaRecorder.stop();
    };
  }, [localVideoStream, isRecording, playbackVideoElementRef]);

  const onPlaybackVideoStart = () => {
    setIsCompositing(true);
  };

  const onPlaybackVideoEnd = () => {
    setIsCompositing(false);
  };

  const onPenStrokes = (history: Line[]) => {
    setLastHistory(history);
  };

  return (
    <>
      <CssBaseline />
      <main className={classes.root}>
        <Container>
          <Typography
            component="h1"
            variant="h5"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            POC Telestration Recording
          </Typography>

          <div className={classes.buttons}>
            {isCompositing ? (
              <CircularProgress size={30} />
            ) : (
              <FormControlLabel
                control={
                  <Switch
                    value={isRecording}
                    onChange={(event) => {
                      const isRecording = event.target.checked;
                      setIsRecording(isRecording);
                      if (isRecording) {
                        setRecordingStartedAt(performance.now());
                      }
                    }}
                  />
                }
                label={isRecording ? "Recording video" : "Record video"}
              />
            )}
          </div>

          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Card>
                <CardHeader title="Video" />
                <CardMedia>
                  <video
                    ref={videoElementRef}
                    muted
                    width={VIDEO_WIDTH}
                    height={VIDEO_HEIGHT}
                    autoPlay
                  />
                </CardMedia>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <CardHeader title="Telestration Canvas" />
                <CardMedia>
                  <TelestrationCanvas
                    width={VIDEO_WIDTH}
                    height={VIDEO_HEIGHT}
                    stream={localVideoStream}
                    isRecording={isRecording}
                    recordingStartedAt={recordingStartedAt}
                    onPenStrokes={onPenStrokes}
                  />
                </CardMedia>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <CardHeader title="Playback Video" />
                <CardMedia>
                  <video
                    ref={playbackVideoElementRef}
                    muted
                    width={VIDEO_WIDTH}
                    height={VIDEO_HEIGHT}
                    autoPlay
                    onPlay={onPlaybackVideoStart}
                    onEnded={onPlaybackVideoEnd}
                  />
                </CardMedia>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <CardHeader title="Composite Canvas" />
                <CardMedia>
                  <CompositeCanvas
                    width={VIDEO_WIDTH}
                    height={VIDEO_HEIGHT}
                    videoElementRef={playbackVideoElementRef}
                    isCompositing={isCompositing}
                    history={lastHistory}
                  />
                </CardMedia>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </main>
    </>
  );
}

export default App;
